import {ExecutableCore} from './executable.core';
import * as fs from 'fs';

//pass in the config; // one day this should be loaded from file
// make this work as a command line argument on startin the core
let configurationFile: string = fs.readFileSync('/Users/michael/.jones/config.json').toString();

let core = new ExecutableCore(JSON.parse(configurationFile));
core.run();
