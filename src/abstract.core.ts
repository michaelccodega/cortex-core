

export abstract class AbstractCore {
    // Public Fields
    public CORE_ID: string = '';
    public path: string = '';

    // Protected Fields
    protected config: object = {};

    // Private Debug fields
    protected logging: boolean = false;

    // Constructor
    constructor(){
        //
    }

    public end(): void {
        //
    }

    protected configure(config: object): void {
        this.config = config;
    }
}