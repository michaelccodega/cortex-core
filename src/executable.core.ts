//
//import * as cortex from 'lib-cortex';
import { PrimativeCore } from './primative.core';
import { AbstractCore } from './abstract.core';
import { IPCManager } from './proxy/ipc.manager';
import { mDnsProxy } from './proxy/mdns.proxy.core';
import { TCPManager } from './proxy/tcp.manager';

export class ExecutableCore extends AbstractCore {

    //
    protected core: PrimativeCore;

    protected useIPC: boolean = true;
    protected ipcManager: IPCManager;

    // This would use mDNS on a local network to look for nodes
    protected usemDNS: boolean = false;
    protected bonjourProxy: mDnsProxy;

    // This is cores that expose themselves upon the Bittorent (or other public, or a combination of) DHT/'s
    //      this enables decentralized communication from peer cores that 
    //      are behind firewalls and otherwise unable to connect

    protected useDHT: boolean = false;
    //protected dhtProxy: DHTProxy;

    // This is a peer with a URL or IP address
    protected useTCPpeer: boolean = false;
    protected tcpManager: TCPManager;

    // This is a peer that offers themselves on IPFS
    protected useIPFSpeer: boolean = false;

    //
    constructor(config: object){
        super();

        // Configure executable
        this.configure(config);

        // create root core
        this.core = new PrimativeCore(config);
        
        // establish IPC proxy
        if(this.useIPC){
            this.ipcManager = new IPCManager(config, this.core);
        }

        if(this.usemDNS){
            // This is not yet implemented
            this.bonjourProxy = new mDnsProxy(config, this.core);
        }

        if(this.useDHT){
            // This is not yet implemented
        }

        if(this.useTCPpeer){
            // THis is not yet implemented
        }

        if(this.useIPFSpeer){
            //this is not yet implemented
        }

    }

    ////////////////////////// PUBLIC METHODS //////////////////////////
    public run(): void {
        //what does this do?
        console.log('\n\n');
        console.log('Starting the CortexCore... ');
        //console.log('   remote connections to additional cores is: NOT ENABLED');
        console.log('\n\n');

        this.establishProcess();
    }

    public end(): void {
        //
        this.core.end();

        //end proc.
        process.exit(0);
    }

    ////////////////////////// PROTECTED METHODS //////////////////////////

    protected configure(config: object): void {
        /////// MANIPULATE THE CONFIG OBJECT AS NEEDED 
        //TODO accept a version number?
        this.config = config;

        /////// SET UP THE CONFIGURATION FOR JONES WORK
        // Check for CORE ID
        if (this.config['jones']['core_id']) {
            console.log('JONES INITIALIZATION - Found Core ID');
            this.CORE_ID = this.config['jones']['core_id'];
            console.log('[Core] setting core id: ', this.CORE_ID);
        } else {
            //this is what its set to on init
            console.log('JONES INITIALIZATION - NO Found Core ID');
            if (this.CORE_ID == '') {
                this.CORE_ID = 'default_core';
                console.log('[Core] setting core id: ', this.CORE_ID);
            } else {
                //do nothing, it has something other than init
                console.log('[Core] preserving old core id: ', this.CORE_ID);
            }
        }

        // Check for PATH
        if (this.config['jones']['path']) {
            console.log('JONES INITIALIZATION - Found path');
            this.path = this.config['jones']['path'];
            console.log('[JONES CORE] setting path: ', this.path);
        } else {
            console.log('JONES INITIALIZATION - NO Found path');
            if (this.path == '') {
                this.path = '/User/Michael/.jones/';
                console.log('[JONES CORE] setting path: ', this.path);
            } else {
                //do nothing, it has something other than init
                console.log('[JONES CORE] preserving old path: ', this.path);
            }
        }

        if(this.config['discovery']['mdns'] == true){
            this.usemDNS = true; 
        } else {
            this.usemDNS = false;
        }

        /////// Set up the Environment
        if(this.config['debug']['log']){
            if(this.config['debug']['log'] == true){
                this.logging = true;
                console.log('logging is enabled');
            }
        } else {
            this.logging = false;
        }
    }

    ////////////////////////// PRIVATE METHODS //////////////////////////

    private establishProcess(): void {
        // Set up the safe ending when the process is killed
        process.on('SIGINT', () => {
            console.log('\n ending \n');
            this.end();
        });

        process.on('SIGTERM', () => {
            console.log('\n ending \n');
            this.end();
        });
    }    
}
