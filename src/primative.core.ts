//Imports
import { AbstractCore } from './abstract.core';
//
import { Node } from './core/node';
import { Core } from './core/core';
import { Service } from './core/service';
//
import * as cortex from 'lib-cortex';
import { Message } from 'lib-cortex/lib/messages';
//import { Message } from 'lib-cortex/lib/messages';


//Main Core
export class PrimativeCore extends AbstractCore {

    // Public Fields
    public CORE_ID: string = '';
    public path: string = '';

    //Fields for Nodes
    protected nodes: { [name: string]: Node } = {};

    //Fields for Cores
    protected cores: {[name: string]: Core } = {};

    //Fields for Services
    protected availableServices: { [name: string]: Service } = {};
    
    // Fields for Events
    protected eventListeners: Array<Function> = [];
    protected localHandler: Function;

    //
    protected config: object = {};
    protected logging: boolean = false;

    //Constructor 
    constructor(config: Object) {
        super();

        //handle the configs
        this.configure(config);
    }

    ////////////////////////// Core's Functionality Methods //////////////////////////

    /**
     * This is used to set callbacks to events that the core will create
     * @param event 
     * @param callback 
     */
    public on(event: string, callback: Function): void {
        switch(event){
            case 'event': {
                this.eventListeners.push(callback);
                break;
            }
            case 'event-local': {
                this.localHandler = callback;
                break;
            }
            default: {
                //
            }
        }

        return;
    }

    /**
     * This is used to message a node, regardless of a location
     * @param nodeHandle
     * @param message
     */
    public messageNode(nodeHandle: string, message: Message): void {
        //TODO This is an area where there should be error handling
        this.nodes[nodeHandle].sendMessage(message);
    }

    /**
     * This is used to message a service, regardless of the location
     * @param serviceHandle 
     * @param message 
     */
    public messageService(serviceHandle: string, message: cortex.Message): void {
        //TODO add error handling
        this.availableServices[serviceHandle].sendMessage(message);
    }

    /**
     * Messaging a core, regardless of location, it will be routed accordingly
     * @param coreHandle 
     * @param message 
     */
    public messageCore(coreHandle: string, message: cortex.Message): void {
        //TODO error handling
        this.cores[coreHandle].sendMessage(message);
    }

    /**
     * This sends this to the various cores as "publish message", also shares as localEvent
     * @param eventHandle 
     * @param messageContent 
     * @param sender //TODO remove sender
     * @param header 
     */
    public publishEvent(eventHandle: string, messageContent: string, sender: string, header: boolean): void {
        let message: cortex.Message;

        //add header to message as needed, this enables wrapping, while not requiring it preventing
        //  annoying recursive messages
        if(header){
            message = cortex.Message.Parse(messageContent);
        }else{
            message = new cortex.PublishMessageBuilder(this.CORE_ID)
            .setEvent(eventHandle)
            .setMessage(messageContent)
            .returnMessage();
        }

        //This should call the list of coures and foward it to other cores
        Object.keys(this.cores).forEach((core)=>{
            this.messageCore(core, message);
        });    
        
        //Also share to local 
        this.publishLocalEvent(eventHandle, messageContent, sender, true);
    }

    /**
     * This should get called by any manager, it publishes to the "LocalHandler" function
     * @param eventHandle 
     * @param message 
     */
    public publishLocalEvent(eventHandle: string, messageContent: string, sender: string, header: boolean): void {
        let message: cortex.Message;

        if(header){
            message = cortex.Message.Parse(messageContent);
        }else{
            message = new cortex.PublishMessageBuilder(this.CORE_ID)
            .setEvent(eventHandle)
            .setMessage(messageContent)
            .returnMessage();
        }
        
        this.localHandler(eventHandle, message);
    }

    public registerNode(node: Node): void {
        if(this.logging){
            console.log('(Primative Core) registering node: ' + node.handle);
        }

        this.nodes[node.handle] = node;
        this.publishLocalEvent('system', 'new node registered: ' + node.handle, this.CORE_ID, false);
    }
    
    public registerService(service: Service): void {
        if(this.logging){
            console.log('(Primative Core) registering service: ' + service.serviceHandle);
        }

        this.availableServices[service.serviceHandle] = service;

        // Error catching here.. in case messages dont go through right
        if(this.nodes[service.parentNode]){
            this.nodes[service.parentNode].availableServices.push(service);
        } else {
            // TODO if something fails...
        }
        

        this.publishLocalEvent('system', 'new service registered: ' + service.serviceHandle, this.CORE_ID, false);
    }

    public registerCore(core: Core): void {
        //TODO add error handling
        this.cores[core.handle] = core;
    }

    ////////////////////////// GETTER SETTER METHODS //////////////////////////

    ////////////////////////// PROTECTED METHODS //////////////////////////
    /**
     * This handles configuration, and sets object fields based on the config object
     * @param config 
     */
    protected configure(config: object): void {
        /////// MANIPULATE THE CONFIG OBJECT AS NEEDED 
        //TODO accept a version number?
        this.config = config;

        /////// SET UP THE Environment
        if(this.config['debug']['log']){
            if(this.config['debug']['log'] == true){
                this.logging = true;
            }
        } else {
            this.logging = false;
        }
    }

    ////////////////////////// PRIVATE METHODS //////////////////////////
}