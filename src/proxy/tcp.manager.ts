//
import { PrimativeCore } from '../primative.core';
import { Manager } from './interface.manager';
//
import { Node } from '../core/node';
import { Service } from '../core/service';
//
import * as nano from 'nanomsg';
import * as cortex from 'lib-cortex';

export class TCPManager implements Manager {

    // Public Fields
    public CORE_ID: string = '';
    public path: string = '';
    //Fields for Core
    public id: string = 'tcp';
    protected core: PrimativeCore;
    protected config: object = {};
    protected logging: boolean = false;
    //Fields for NanoMsg IPC Socket
    private tcpAddress: string = '';
    private tcpSocket: nano.Socket;

    //constructor
    constructor(config: object, core: PrimativeCore){
        this.core = core;
        this.configure(config);

        // Make a socket
        this.tcpAddress = 'tcp://127.0.0.1:5678';
        this.tcpSocket = nano.socket('rep');
        this.tcpSocket.bind(this.tcpAddress);
        this.tcpSocket.on('data', (data) => {
            this.tcpSocket.send(this.handleMessage(data.toString()));
        });

        this.core.on('event', (eventHandle: string, message: cortex.Message, sender: string) => {
            //sender? - IPC is special?
            if(sender == this.id){
                //
            } else {
                this.publishEvent(eventHandle, message);
            }      
        });
    }

    public end(): void {
        
        //this should free all the other addresses associated with subscriptions and services
    }

    //////////////////////////  Methods //////////////////////////
    /**
     * 
     * @param msg 
     */
    public handleMessage(msg: string): string {
        if (this.logging) {
            console.log('\nCore recieved a new message: ');
            console.log('   ' + msg);
            console.log('');
        }
        let message;
        try {
            message = JSON.parse(msg);
        } catch (error) {
            return this.handleUnknownMessage({});
        }

        if (!message['type']) {
            return this.handleUnknownMessage(message['data']);
        } else {
            switch (message['type']) {
                case 'register': {
                    return this.handleRegisterNode(message['data']);
                }
                case 'publish': {
                    return this.handlePublishMessage(message['data']);
                }
                case 'subscription': {
                    return this.handleSubscriptionMessage(message['data']);
                }
                case 'request': {
                    return this.handleRequest(message['data']);
                }
                case 'register-service': {
                    return this.handleRegisterService(message['data']);
                }
                default: {
                    return this.handleUnknownMessage(message['data']);
                }
            }
        }
    }

    //
    /**
     * 
     * @param config 
     */
    protected configure(config: object): void {
        this.config = config;
        
        //
    }

    /**
     * 
     * @param eventHandle 
     * @param message 
     */
    protected publishEvent(eventHandle: string, message: cortex.Message): void {
        //send to the cores that it is responsible for
    }

    ////////////////////////// IPC Event Handler Methods //////////////////////////

    /**
     * 
     * @param msgData 
     */
    protected handleRegisterNode(msgData: object): string {
        if (this.logging) {
            console.log('[Core] recieved a register message from ' + msgData['nodeHandle']);
        }

        let node = new TCPNode(msgData['nodeHandle'], msgData['uuid'], msgData['parentCoreId']);
        this.core.registerNode(node);

        return ((new cortex.AckMessageBuilder(this.core.CORE_ID))
            .returnMessageString()
        );
    }

    /**
     * 
     * @param msg 
     */
    protected handlePublishMessage(msg: object): string {
        let eventHandle: string = msg['event'];
        let messageContent: string = msg['message'];

        this.core.publishLocalEvent(eventHandle, messageContent, this.id, true);

        return (new cortex.AckMessageBuilder('core')).returnMessageString();
    }

    /**
     * 
     * @param msg 
     */
    protected handleSubscriptionMessage(msg: object): string {
        //handle the information

        return (new cortex.AckMessageBuilder(this.CORE_ID)).returnMessageString();
    }

    /**
     * 
     * @param msg 
     */
    protected handleRequest(msg: object): string {
        //TODO handle this
        // if the request type is 'get local nodes' => return list of nodes
        // if the request type is 'get local services' => return list of services
        // if the request type is 'get subscribe to this: X' => sub + ack (this should be limited to once)
        //      There should be a level of consistency across nodes and their messages, so things are sent once?
        // if the request type is 'new publish event' => publish to the right event on IPC
        //
        if (msg['type'] == 'request') {
            this.core.messageService(msg['destination'], cortex.Message.Parse(msg.toString()));
        }

        return this.handleUnknownMessage(msg);
    }

    /**
     * 
     * @param msg 
     */
    protected handleRegisterService(msg: object): string {
        //
        if (this.logging) {
            //
        }

        let service = new TCPService(msg['serviceHandle'], msg['params'], msg['parentNode']);
        this.core.registerService(service);

        return ((new cortex.AckMessageBuilder('core'))
            .returnMessageString()
        );
    }

    /**
     * 
     * @param msg 
     */
    protected handleUnknownMessage(message: object): string {
        if (this.logging) {
            console.log('[core] unknown message: ' + JSON.stringify(message));
        }

        return ((new cortex.WarnMessageBuilder('core'))
            .warning('unknown')
            .returnMessageString()
        );
    }

}

export class TCPNode extends Node {

    //
    private socket: nano.Socket;
    private address: string;

    constructor(nodeHandle: string, uuid: string, coreId: string){
        //TODO: change this to not be for IPC
        super(nodeHandle, uuid, [], coreId);
        
        this.address = cortex.IPCUtils.encodeAddressNode(this.handle);
        this.socket = cortex.SocketUtils.createSocketSlave('req', this.address);
    }

    public sendMessage(message: cortex.Message): void {
        this.socket.send(message.toString());
    }
}

export class TCPService extends Service {
    //
    private socket: nano.Socket;
    private address: string;

    constructor(serviceHandle: string, params: object, parentNode: string){
        //TODO: change this to not be for IPC
        super(serviceHandle, params, parentNode);
        
        this.address = cortex.IPCUtils.encodeAddressService(this.serviceHandle);
        this.socket = cortex.SocketUtils.createSocketSlave('req', this.address);
    }

    public sendMessage(message: cortex.Message): void {
        //this.socket.send(message.toString());

        let msg: cortex.Message = new cortex.ForwardMessageBuilder('tcp')
            .setMessage(message)
            .returnMessage();
    }
}