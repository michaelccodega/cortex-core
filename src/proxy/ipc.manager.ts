//
import { PrimativeCore } from '../primative.core';
import { Manager } from './interface.manager';
//
import { Node } from '../core/node';
import { Service } from '../core/service';
//
import * as nano from 'nanomsg';
import * as cortex from 'lib-cortex';

export class IPCManager implements Manager {

    // Public Fields
    public CORE_ID: string = '';
    public path: string = '';
    //Fields for Core
    public id: string = 'ipc';
    protected core: PrimativeCore;
    protected config: object = {};
    protected logging: boolean = false;
    //Fields for NanoMsg IPC Socket
    private ipcAddress: string = '';
    private ipcSocket: nano.Socket;

    private eventBuses: { [name: string]: any } = {};

    // Constructor
    constructor(config: object, core: PrimativeCore) {
        this.core = core;
        this.configure(config);
        this.path = this.core.path;

        this.eventBuses = {};

        // Establish IPC
        cortex.SocketUtils.freeIPCfile(this.ipcAddress, this.core.path);
        this.ipcSocket = cortex.SocketUtils.createSocketMaster('rep', this.ipcAddress);
        this.ipcSocket.on('data', (data) => {
            //console.log('\'' + data.toString() + '\'');
            this.ipcSocket.send(this.handleMessage(data.toString()));
        });

        this.core.on('event', (eventHandle: string, message: cortex.Message, sender: string) => {
            //sender? - IPC is special?
            if(sender == 'ipc'){
                //
            } else {
                //
            }
            this.handleLocalPublishEvent(eventHandle, message);
        });

        this.core.on('event-local', (eventHandle: string, message: cortex.Message) => {
            //sender :/
            this.handleLocalPublishEvent(eventHandle, message);
        });

    }

    public end(): void {
        this.ipcSocket.shutdown(cortex.SocketUtils.pathToSocketAddress(this.ipcAddress));
        this.ipcSocket.close();

        cortex.SocketUtils.freeIPCfile(this.ipcAddress, this.path);
        //this should free all the other addresses associated with subscriptions and services
    }

    //////////////////////////  Methods //////////////////////////

    /**
     * Takes in all the IPC messages, and delegates based on the event type
     * @param msg this is the message taken in, as a string (to be JSON-ified)
     */
    public handleMessage(msg: string): string {
        if (this.logging) {
            console.log('\nCore recieved a new message: ');
            console.log('   ' + msg);
            console.log('');
        }
        let message;
        try {
            message = JSON.parse(msg);
        } catch (error) {
            return this.handleUnknownMessage({});
        }

        if (!message['type']) {
            return this.handleUnknownMessage(message['data']);
        } else {
            switch (message['type']) {
                case 'register': {
                    return this.handleRegisterNode(message['data']);
                }
                case 'publish': {
                    return this.handlePublishMessage(message['data']);
                }
                case 'subscription': {
                    return this.handleSubscriptionMessage(message['data']);
                }
                case 'request': {
                    return this.handleRequest(message['data']);
                }
                case 'register-service': {
                    return this.handleRegisterService(message['data']);
                }
                default: {
                    return this.handleUnknownMessage(message['data']);
                }
            }
        }
    }

    protected configure(config: object): void {
        /////// MANIPULATE THE CONFIG OBJECT AS NEEDED 
        //TODO accept a version number?
        this.config = config;
        /////// SET UP THE CONFIGURATION FOR IPC
        if (this.config['ipc']['address']) {
            console.log('JONES INITIALIZATION - Found IPC address');
            this.ipcAddress = this.config['ipc']['address'];
            console.log('[JONES CORE] setting IPC address: ', this.ipcAddress);
        } else {
            console.log('JONES INITIALIZATION - NO Found IPC Address');
            if (this.ipcAddress == '') {
                this.ipcAddress = '/tmp/jones_core.ipc';
                console.log('[Core] setting IPC address: ', this.ipcAddress);
            } else {
                console.log('[Core] preserving old IPC address: ', this.ipcAddress);
            }
        }

        /////// Set up the Environment
        if (this.config['debug']['log']) {
            if (this.config['debug']['log'] == true) {
                this.logging = true;
                console.log('logging is enabled');
            }
        } else {
            this.logging = false;
        }

    }

    protected newEventBus(eventHandle: string): void {
        if (this.logging) {
            console.log('[Core] creating a new EventBus for: ' + eventHandle);
        }

        let address: string = cortex.IPCUtils.encodeAddressEvent(eventHandle);
        let socket: nano.Socket = cortex.SocketUtils.createSocketMaster('bus', address);

        socket.on('data', (message) => {
            //this should never trigger? // Since no where to SLAVE sockets send messages
            if (this.logging) {
                console.log(message);
            }
        });

        this.eventBuses[eventHandle] = {
            eventHandle: eventHandle,
            address: address,
            socket: socket
        };
    }

    /**
     * This sends the published message to the eventBus's. It is the handler that is passed into the P.Core
     * @param eventHandle 
     * @param message 
     */
    protected handleLocalPublishEvent(eventHandle: string, message: cortex.Message): void {
        this.eventBuses[eventHandle].socket.send(message.toString());
        console.log('eventBus Address: ' + this.eventBuses[eventHandle]['address']);

        if (this.logging) {
            console.log('Core - Publishing Event ' + eventHandle + ' @= ' + message.toString());
            console.log('event bus is: ' + this.eventBuses[eventHandle].address);
        }
    }

    ////////////////////////// IPC Event Handler Methods //////////////////////////

    /**
     * 
     * @param msgData 
     */
    private handleRegisterNode(msgData: object): string {
        if (this.logging) {
            console.log('[Core] recieved a register message from ' + msgData['nodeHandle']);
        }

        let node = new IPCNode(msgData['nodeHandle'], msgData['uuid'], msgData['parentCoreId']);
        this.core.registerNode(node);

        return ((new cortex.AckMessageBuilder(this.core.CORE_ID))
            .returnMessageString()
        );
    }

    /**
     * This takes in a publish message from the central IPC channel, and pushes it as a new event to the P.Core
     * @param msg the publish message
     */
    private handlePublishMessage(msg: object): string {
        /*

        This should tell the core about the message, though "Publish Message"
        It should be use the "local" to know when to publish locally

        */
        let eventHandle: string = msg['event'];
        let messageContent: string = msg['message'];

        //Generating an event bus for this message if it does not exist 
        // should this be done here, or in a handler
        if (this.eventBuses[eventHandle]) {
            // this event is already something we have a socket for, yay!
        } else {
            // Somewhere they should encode the event handle, but it should be as close to the socket address thing as possible
            this.newEventBus(eventHandle);
        }

        this.core.publishEvent(eventHandle, messageContent, this.id, true);

        return (new cortex.AckMessageBuilder('core')).returnMessageString();
    }

    /**
     * 
     * @param msg 
     */
    private handleSubscriptionMessage(msg: object): string {
        //handle the information
        if (this.logging) {
            console.log('[Core] recieved a new subscription message: ' + JSON.stringify(msg));
        }
        let eventHandle = msg['eventHandle'];
        if (this.eventBuses[eventHandle]) {
            // this event is already something we have a socket for, yay!
        } else {
            this.newEventBus(eventHandle);
        }

        return (new cortex.AckMessageBuilder(this.CORE_ID)).returnMessageString();
    }

    /**
     * 
     * @param msg 
     */
    private handleRequest(msg: object): string {
        //TODO handle this
        // if the request type is 'get local nodes' => return list of nodes
        // if the request type is 'get local services' => return list of services
        // if the request type is 'get subscribe to this: X' => sub + ack (this should be limited to once)
        //      There should be a level of consistency across nodes and their messages, so things are sent once?
        // if the request type is 'new publish event' => publish to the right event on IPC
        //
        if (msg['type'] == 'request') {
            this.core.messageService(msg['destination'], cortex.Message.Parse(msg.toString()));
        }

        return this.handleUnknownMessage(msg);
    }

    /**
     * 
     * @param msg 
     */
    private handleRegisterService(msg: object): string {
        //
        if (this.logging) {
            //
        }

        let service = new IPCService(msg['serviceHandle'], msg['params'], msg['parentNode']);
        this.core.registerService(service);

        return ((new cortex.AckMessageBuilder('core'))
            .returnMessageString()
        );
    }

    /**
     * 
     * @param message 
     */
    private handleUnknownMessage(message: object): string {
        if (this.logging) {
            console.log('[core] unknown message: ' + JSON.stringify(message));
        }

        return ((new cortex.WarnMessageBuilder('core'))
            .warning('unknown')
            .returnMessageString()
        );
    }
}

export class IPCNode extends Node {

    //
    private socket: nano.Socket;
    private address: string;

    constructor(nodeHandle: string, uuid: string, coreId: string) {
        super(nodeHandle, uuid, [], coreId);

        this.address = cortex.IPCUtils.encodeAddressNode(this.handle);
        this.socket = cortex.SocketUtils.createSocketSlave('req', this.address);
    }

    public sendMessage(message: cortex.Message): void {
        this.socket.send(message.toString());
    }
}

export class IPCService extends Service {
    //
    private socket: nano.Socket;
    private address: string;

    constructor(serviceHandle: string, params: object, parentNode: string) {
        super(serviceHandle, params, parentNode);

        this.address = cortex.IPCUtils.encodeAddressService(this.serviceHandle);
        this.socket = cortex.SocketUtils.createSocketSlave('req', this.address);
    }

    public sendMessage(message: cortex.Message): void {
        this.socket.send(message.toString());
    }
}