//Imports
import { AbstractProxy } from './abstract.proxy.core';
import * as bonjour from 'bonjour';
import { PrimativeCore } from '../primative.core';

/*

This should find any other TCP Cores on the network, and expose them as TCP Proxy's

Then, it publishes itself.  It creates a TCP listner in the mDNS proxy, and that handles incoming TCP Messages

*/

export class mDnsProxy extends AbstractProxy {

    // Fields for Discovery
    private bonjour: any; //TODO get bonjour types
    private id: string;

    private debugLog: boolean = false;

    // Constructor
    constructor(config: object, core: PrimativeCore) {
        super(core);
        this.config = config;
        this.configure();

        this.id = 'cortex-core-' + coreHandle;
        this.bonjour = bonjour();
        this.bonjour.publish({
            name: this.id,
            type: 'cortex-core',
            port: 9876,
            txt: {}
        });
    }

    public spin(): void {
        /*
            This repeats once a second, looking for new cores
        */
        setInterval(() => {
            this.bonjour.find({ type: 'cortex-core' }, (service) => {
                if(service['name']==this.id){
                    //
                } else {
                    if(this.debugLog){
                        console.log('\n[proxy-core] new Bonjour service discovered: ' + JSON.stringify(service));
                    }
                }
            });
        }, 1000);
    }

    protected configure(): void {
        if(this.config['debug']['log'] == true){
            this.debugLog = true;
        } else {
            this.debugLog = false;
        }




    }
}