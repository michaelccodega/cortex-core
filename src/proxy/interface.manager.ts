//

export interface Manager {

    handleMessage(msg: string): string;
    end(): void;
}