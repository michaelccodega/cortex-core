// Imports
import * as cortex from 'lib-cortex';

export abstract class Service {
    
    //
    public serviceHandle: string;
    public params: object;
    public parentNode: string;

    constructor(serviceHandle: string, params: object, parentNode: string){
        this.serviceHandle = serviceHandle;
        this.params = params;
        this.parentNode = parentNode;
    }

    public abstract sendMessage(message: cortex.Message): void;

}