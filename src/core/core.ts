// Imports
import * as cortex from 'lib-cortex';


export abstract class Core {
    
    //Fields that define a node
    public handle: string;
    public uuid: string;

    constructor(handle: string, uuid: string){
        this.handle = handle;
        this.uuid = uuid;
    }

    public abstract sendMessage(message: cortex.Message): void;

}