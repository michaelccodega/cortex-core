// Imports
import * as cortex from 'lib-cortex';
import { Service } from './service';

export abstract class Node {
    
    //Fields that define a node
    public handle: string;
    public uuid: string;
    public availableServices: Array<Service>;
    public parentCoreId: string;

    constructor(handle: string, uuid: string ,availableServices: Array<Service>, parentCoreId: string){
        this.handle = handle;
        this.uuid = uuid;
        this.availableServices = availableServices;
        this.parentCoreId = parentCoreId;
    }

    public abstract sendMessage(message: cortex.Message): void;

}