//////////////////////////////////////////////////////////////
//                                                          //
//  Gulp file for Cortex-Core build                         //
//  something special here... c2017                         //
//////////////////////////////////////////////////////////////
//requires
var gulp = require('gulp');
var gutil = require('gulp-util');
var tslint = require("gulp-tslint");
var tsc = require('gulp-typescript');
var sourcemaps = require('gulp-sourcemaps');

//////////////////////////////////////////////////////////////
//  GULP CONFIGURATION
//////////////////////////////////////////////////////////////
//config
var config = {
    allTypeScript: 'src/*.ts',
    developmentConfig: 'development/devel.json'
};

var tsProject = tsc.createProject('tsconfig.json');

////////////////////////////////////////////////////////////
//  MASTER TASKS
////////////////////////////////////////////////////////////

gulp.task('default', ['load','watch']);

gulp.task('load', [
    'log-start',
    'ts'
]);

gulp.task('ts', [
    'generate-ts-project',
    'ts-lint',
    'compile-ts'
]);

////////////////////////////////////////////////////////////
//  WATCH TASKS
////////////////////////////////////////////////////////////
gulp.task('watch', function() {
    
    //watch for typescript files changing
    gulp.watch(config.allTypeScript, ['ts']);
    //watch the gitignore 
    gulp.watch('.gitignore', ['check-gitignore']);
    //watch the npm ingore
    gulp.watch('.npmignore', ['check-npmignore']);
    //watch the package.json
    gulp.watch('package.json', ['ckeck-package-json']);
    //watch the tsconfig
    gulp.watch('tsconfig.json', []);
    //wtch the tslint

});

////////////////////////////////////////////////////////////
//  TYPESCRIPT TASKS
////////////////////////////////////////////////////////////
// typescript compiler
gulp.task('compile-ts', () => {
    var sourceTsFiles = [config.allTypeScript];//path to typescript files
    var tsResult = gulp.src(sourceTsFiles)
        .pipe(sourcemaps.init())
        .pipe(tsc(tsProject));

        tsResult.dts.pipe(gulp.dest(config.tsOutputPath));
        return tsResult.js
                        .pipe(sourcemaps.write('.'))
                        .pipe(gulp.dest(config.tsOutputPath));
});

// This is the Typescript linter
gulp.task('ts-lint', function () {
    gulp.src('src/*.ts')
        .pipe(tslint({
            formatter: "stylish"
        }))
        // removing this supresses a lot of output
        .pipe(tslint.report());
});

gulp.task('ts', () => {
    // This gets called on every TS file change

});

////////////////////////////////////////////////////////////
//  VARIOUS TASKS
////////////////////////////////////////////////////////////

gulp.task('log-start', () => {
    gutil.log('======== STARTING DEVELOPMENT ENVIRONMENT ========');
});

////////////////////////////////////////////////////////////
//  CHECK FILES FOR INTEGRITY
////////////////////////////////////////////////////////////

gulp.task('check-package-json', () => {
    //TODO
});

gulp.task('check-gitignore', () => {
    //this should read any ignore'ed file types and store it
    // in some list somewhere
    // if the gitignore file goes missing, replace and repopulate
});

gulp.task('check-npmignore', () => {
    //this should read any ignore'ed file types and store it
    // in some list somewhere
    // if the npmignore file goes missing, replace and repopulate
});

gulp.task('generate-ts-project', () => {
    tsProject = tsc.createProject('tsconfig.json');
});

