"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
//
//import * as cortex from 'lib-cortex';
var primative_core_1 = require("./primative.core");
var abstract_core_1 = require("./abstract.core");
var ipc_manager_1 = require("./proxy/ipc.manager");
var mdns_proxy_core_1 = require("./proxy/mdns.proxy.core");
var ExecutableCore = /** @class */ (function (_super) {
    __extends(ExecutableCore, _super);
    //
    function ExecutableCore(config) {
        var _this = _super.call(this) || this;
        _this.useIPC = true;
        // This would use mDNS on a local network to look for nodes
        _this.usemDNS = false;
        // This is cores that expose themselves upon the Bittorent (or other public, or a combination of) DHT/'s
        //      this enables decentralized communication from peer cores that 
        //      are behind firewalls and otherwise unable to connect
        _this.useDHT = false;
        //protected dhtProxy: DHTProxy;
        // This is a peer with a URL or IP address
        _this.useTCPpeer = false;
        // This is a peer that offers themselves on IPFS
        _this.useIPFSpeer = false;
        // Configure executable
        _this.configure(config);
        // create root core
        _this.core = new primative_core_1.PrimativeCore(config);
        // establish IPC proxy
        if (_this.useIPC) {
            _this.ipcManager = new ipc_manager_1.IPCManager(config, _this.core);
        }
        if (_this.usemDNS) {
            // This is not yet implemented
            _this.bonjourProxy = new mdns_proxy_core_1.mDnsProxy(config, _this.core);
        }
        if (_this.useDHT) {
            // This is not yet implemented
        }
        if (_this.useTCPpeer) {
            // THis is not yet implemented
        }
        if (_this.useIPFSpeer) {
            //this is not yet implemented
        }
        return _this;
    }
    ////////////////////////// PUBLIC METHODS //////////////////////////
    ExecutableCore.prototype.run = function () {
        //what does this do?
        console.log('\n\n');
        console.log('Starting the CortexCore... ');
        //console.log('   remote connections to additional cores is: NOT ENABLED');
        console.log('\n\n');
        this.establishProcess();
    };
    ExecutableCore.prototype.end = function () {
        //
        this.core.end();
        //end proc.
        process.exit(0);
    };
    ////////////////////////// PROTECTED METHODS //////////////////////////
    ExecutableCore.prototype.configure = function (config) {
        /////// MANIPULATE THE CONFIG OBJECT AS NEEDED 
        //TODO accept a version number?
        this.config = config;
        /////// SET UP THE CONFIGURATION FOR JONES WORK
        // Check for CORE ID
        if (this.config['jones']['core_id']) {
            console.log('JONES INITIALIZATION - Found Core ID');
            this.CORE_ID = this.config['jones']['core_id'];
            console.log('[Core] setting core id: ', this.CORE_ID);
        }
        else {
            //this is what its set to on init
            console.log('JONES INITIALIZATION - NO Found Core ID');
            if (this.CORE_ID == '') {
                this.CORE_ID = 'default_core';
                console.log('[Core] setting core id: ', this.CORE_ID);
            }
            else {
                //do nothing, it has something other than init
                console.log('[Core] preserving old core id: ', this.CORE_ID);
            }
        }
        // Check for PATH
        if (this.config['jones']['path']) {
            console.log('JONES INITIALIZATION - Found path');
            this.path = this.config['jones']['path'];
            console.log('[JONES CORE] setting path: ', this.path);
        }
        else {
            console.log('JONES INITIALIZATION - NO Found path');
            if (this.path == '') {
                this.path = '/User/Michael/.jones/';
                console.log('[JONES CORE] setting path: ', this.path);
            }
            else {
                //do nothing, it has something other than init
                console.log('[JONES CORE] preserving old path: ', this.path);
            }
        }
        if (this.config['discovery']['mdns'] == true) {
            this.usemDNS = true;
        }
        else {
            this.usemDNS = false;
        }
        /////// Set up the Environment
        if (this.config['debug']['log']) {
            if (this.config['debug']['log'] == true) {
                this.logging = true;
                console.log('logging is enabled');
            }
        }
        else {
            this.logging = false;
        }
    };
    ////////////////////////// PRIVATE METHODS //////////////////////////
    ExecutableCore.prototype.establishProcess = function () {
        var _this = this;
        // Set up the safe ending when the process is killed
        process.on('SIGINT', function () {
            console.log('\n ending \n');
            _this.end();
        });
        process.on('SIGTERM', function () {
            console.log('\n ending \n');
            _this.end();
        });
    };
    return ExecutableCore;
}(abstract_core_1.AbstractCore));
exports.ExecutableCore = ExecutableCore;
//# sourceMappingURL=executable.core.js.map