"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AbstractCore = /** @class */ (function () {
    // Constructor
    function AbstractCore() {
        // Public Fields
        this.CORE_ID = '';
        this.path = '';
        // Protected Fields
        this.config = {};
        // Private Debug fields
        this.logging = false;
        //
    }
    AbstractCore.prototype.end = function () {
        //
    };
    AbstractCore.prototype.configure = function (config) {
        this.config = config;
    };
    return AbstractCore;
}());
exports.AbstractCore = AbstractCore;
//# sourceMappingURL=abstract.core.js.map