"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Node = /** @class */ (function () {
    function Node(handle, uuid, availableServices, parentCoreId) {
        this.handle = handle;
        this.uuid = uuid;
        this.availableServices = availableServices;
        this.parentCoreId = parentCoreId;
    }
    return Node;
}());
exports.Node = Node;
//# sourceMappingURL=node.js.map