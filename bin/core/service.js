"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Service = /** @class */ (function () {
    function Service(serviceHandle, params, parentNode) {
        this.serviceHandle = serviceHandle;
        this.params = params;
        this.parentNode = parentNode;
    }
    return Service;
}());
exports.Service = Service;
//# sourceMappingURL=service.js.map