"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Core = /** @class */ (function () {
    function Core(handle, uuid) {
        this.handle = handle;
        this.uuid = uuid;
    }
    return Core;
}());
exports.Core = Core;
//# sourceMappingURL=core.js.map