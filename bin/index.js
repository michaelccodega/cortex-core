"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var executable_core_1 = require("./executable.core");
var fs = require("fs");
//pass in the config; // one day this should be loaded from file
// make this work as a command line argument on startin the core
var configurationFile = fs.readFileSync('/Users/michael/.jones/config.json').toString();
var core = new executable_core_1.ExecutableCore(JSON.parse(configurationFile));
core.run();
//# sourceMappingURL=index.js.map