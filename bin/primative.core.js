"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
//Imports
var abstract_core_1 = require("./abstract.core");
//
var cortex = require("lib-cortex");
//import { Message } from 'lib-cortex/lib/messages';
//Main Core
var PrimativeCore = /** @class */ (function (_super) {
    __extends(PrimativeCore, _super);
    //Constructor 
    function PrimativeCore(config) {
        var _this = _super.call(this) || this;
        // Public Fields
        _this.CORE_ID = '';
        _this.path = '';
        //Fields for Nodes
        _this.nodes = {};
        //Fields for Cores
        _this.cores = {};
        //Fields for Services
        _this.availableServices = {};
        // Fields for Events
        _this.eventListeners = [];
        //
        _this.config = {};
        _this.logging = false;
        //handle the configs
        _this.configure(config);
        return _this;
    }
    ////////////////////////// Core's Functionality Methods //////////////////////////
    /**
     * This is used to set callbacks to events that the core will create
     * @param event
     * @param callback
     */
    PrimativeCore.prototype.on = function (event, callback) {
        switch (event) {
            case 'event': {
                this.eventListeners.push(callback);
                break;
            }
            case 'event-local': {
                this.localHandler = callback;
                break;
            }
            default: {
                //
            }
        }
        return;
    };
    /**
     * This is used to message a node, regardless of a location
     * @param nodeHandle
     * @param message
     */
    PrimativeCore.prototype.messageNode = function (nodeHandle, message) {
        //TODO This is an area where there should be error handling
        this.nodes[nodeHandle].sendMessage(message);
    };
    /**
     * This is used to message a service, regardless of the location
     * @param serviceHandle
     * @param message
     */
    PrimativeCore.prototype.messageService = function (serviceHandle, message) {
        //TODO add error handling
        this.availableServices[serviceHandle].sendMessage(message);
    };
    /**
     * Messaging a core, regardless of location, it will be routed accordingly
     * @param coreHandle
     * @param message
     */
    PrimativeCore.prototype.messageCore = function (coreHandle, message) {
        //TODO error handling
        this.cores[coreHandle].sendMessage(message);
    };
    /**
     * This sends this to the various cores as "publish message", also shares as localEvent
     * @param eventHandle
     * @param messageContent
     * @param sender //TODO remove sender
     * @param header
     */
    PrimativeCore.prototype.publishEvent = function (eventHandle, messageContent, sender, header) {
        var _this = this;
        var message;
        //add header to message as needed, this enables wrapping, while not requiring it preventing
        //  annoying recursive messages
        if (header) {
            message = cortex.Message.Parse(messageContent);
        }
        else {
            message = new cortex.PublishMessageBuilder(this.CORE_ID)
                .setEvent(eventHandle)
                .setMessage(messageContent)
                .returnMessage();
        }
        //This should call the list of coures and foward it to other cores
        Object.keys(this.cores).forEach(function (core) {
            _this.messageCore(core, message);
        });
        //Also share to local 
        this.publishLocalEvent(eventHandle, messageContent, sender, true);
    };
    /**
     * This should get called by any manager, it publishes to the "LocalHandler" function
     * @param eventHandle
     * @param message
     */
    PrimativeCore.prototype.publishLocalEvent = function (eventHandle, messageContent, sender, header) {
        var message;
        if (header) {
            message = cortex.Message.Parse(messageContent);
        }
        else {
            message = new cortex.PublishMessageBuilder(this.CORE_ID)
                .setEvent(eventHandle)
                .setMessage(messageContent)
                .returnMessage();
        }
        this.localHandler(eventHandle, message);
    };
    PrimativeCore.prototype.registerNode = function (node) {
        if (this.logging) {
            console.log('(Primative Core) registering node: ' + node.handle);
        }
        this.nodes[node.handle] = node;
        this.publishLocalEvent('system', 'new node registered: ' + node.handle, this.CORE_ID, false);
    };
    PrimativeCore.prototype.registerService = function (service) {
        if (this.logging) {
            console.log('(Primative Core) registering service: ' + service.serviceHandle);
        }
        this.availableServices[service.serviceHandle] = service;
        // Error catching here.. in case messages dont go through right
        if (this.nodes[service.parentNode]) {
            this.nodes[service.parentNode].availableServices.push(service);
        }
        else {
            // TODO if something fails...
        }
        this.publishLocalEvent('system', 'new service registered: ' + service.serviceHandle, this.CORE_ID, false);
    };
    PrimativeCore.prototype.registerCore = function (core) {
        //TODO add error handling
        this.cores[core.handle] = core;
    };
    ////////////////////////// GETTER SETTER METHODS //////////////////////////
    ////////////////////////// PROTECTED METHODS //////////////////////////
    /**
     * This handles configuration, and sets object fields based on the config object
     * @param config
     */
    PrimativeCore.prototype.configure = function (config) {
        /////// MANIPULATE THE CONFIG OBJECT AS NEEDED 
        //TODO accept a version number?
        this.config = config;
        /////// SET UP THE Environment
        if (this.config['debug']['log']) {
            if (this.config['debug']['log'] == true) {
                this.logging = true;
            }
        }
        else {
            this.logging = false;
        }
    };
    return PrimativeCore;
}(abstract_core_1.AbstractCore));
exports.PrimativeCore = PrimativeCore;
//# sourceMappingURL=primative.core.js.map