"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
//Imports
var abstract_core_1 = require("./abstract.core");
var mdns_proxy_core_1 = require("./proxy/mdns.proxy.core");
//import * as fs from 'fs';
var cortex = require("lib-cortex");
//import { SocketUtils } from 'lib-cortex';
//import { IPCUtils } from 'lib-cortex';
//import { AckMessageBuilder } from 'lib-cortex';
//import { WarnMessageBuilder } from 'lib-cortex';
//Main Core
var Core = /** @class */ (function (_super) {
    __extends(Core, _super);
    //Constructor 
    function Core(config) {
        var _this = _super.call(this) || this;
        //Fields for local nodes
        /*
            Local Nodes include:
            name: string
            availableServices: Array<string>
            subscriptions: Array<string>
        */
        _this.localNodes = {};
        //Fields for Services
        /*
            services includes:
            serviceHandle (key)
                params[]
                nodeHandle
    
        */
        _this.availableServices = {};
        //private messagesInTransit: { [id: string]: any } = {};
        //private routingTable: {[name: string]: any} = {};
        //Fields for Events
        /*
            Subscriptions includes:
            eventHandle
            socketAddress
            socket
        */
        _this.subscriptions = {};
        //Fields for Core to Core communication
        /*
            Cores includes:
            coreHandle
            nodes[] = [{services[], ...}]
    
        */
        /*
            //
        */
        _this.cores = {};
        // This would use mDNS on a local network to look for nodes
        _this.usemDNS = false;
        // This is cores that expose themselves upon the Bittorent (or other public, or a combination of) DHT/'s
        //      this enables decentralized communication from peer cores that 
        //      are behind firewalls and otherwise unable to connect
        _this.useDHT = false;
        // This is a peer with a URL or IP address
        //private useExposedPeer: boolean = false;
        // This is a peer that offers themselves on IPFS
        _this.useIPFSpeer = false;
        //Fields for NanoMsg IPC Socket
        _this.ipcAddress = '';
        // Debug
        _this.logging = false;
        //basic stuff:
        _this.establishProcess();
        //handle the configs
        _this.configure(config);
        //Create listener for NanoMsg - Local
        cortex.SocketUtils.freeIPCfile(_this.ipcAddress, _this.path);
        _this.ipcSocket = cortex.SocketUtils.createSocketMaster('rep', _this.ipcAddress);
        _this.ipcSocket.on('data', function (data) {
            //console.log('\'' + data.toString() + '\'');
            _this.ipcSocket.send(_this.handleIPCMessage(data.toString()));
        });
        if (_this.usemDNS) {
            _this.cores['mdns'] = new mdns_proxy_core_1.mDnsProxy(_this.CORE_ID, _this.config);
            _this.cores['mdns'].spin();
            //this.cores['mdns'].getLocal = () => {};
        }
        if (_this.useDHT) {
            // This is not yet implemented
        }
        if (_this.useIPFSpeer) {
            //this is not yet implemented
        }
        return _this;
    }
    ////////////////////////// PUBLIC METHODS //////////////////////////
    Core.prototype.run = function () {
        //what does this do?
        console.log('\n\n');
        console.log('Starting the CortexCore... ');
        console.log('   listening at IPC addres: ' + this.ipcAddress);
        console.log('   remote connections to additional cores is: NOT ENABLED');
        console.log('\n\n');
    };
    Core.prototype.end = function () {
        //
        this.ipcSocket.shutdown(cortex.SocketUtils.pathToSocketAddress(this.ipcAddress));
        this.ipcSocket.close();
        cortex.SocketUtils.freeIPCfile(this.ipcAddress, this.path);
        //this should free all the other addresses associated with subscriptions and services
        //end proc.
        process.exit(0);
    };
    ////////////////////////// Core's Functionality Methods //////////////////////////
    Core.prototype.publishEvent = function (eventHandle, messageContent) {
        var message = new cortex.PublishMessageBuilder(this.CORE_ID)
            .setEvent(eventHandle)
            .setMessage(messageContent)
            .returnMessage();
        this.subscriptions[eventHandle].socket.send(message.toString());
    };
    Core.prototype.newEventBus = function (eventHandle) {
        if (eventHandle == undefined) {
            return;
        }
        if (this.logging) {
            console.log('[Core] creating a new EventBus for: ' + eventHandle);
        }
        var address = cortex.IPCUtils.encodeAddressEvent(eventHandle);
        var socket = cortex.SocketUtils.createSocketMaster('bus', address);
        socket.on('data', function (message) {
            //this should never trigger? // Since no where to SLAVE sockets send messages
            //
        });
        this.subscriptions[eventHandle] = {
            eventHandle: eventHandle,
            address: address,
            socket: socket
        };
    };
    Core.prototype.registerService = function (serviceHandle, origin, params, local) {
        if (this.logging) {
            console.log('registering another service: ' + serviceHandle);
        }
        this.publishEvent('system', 'new service registered: ' + serviceHandle);
        this.availableServices[serviceHandle] = {
            serviceHandle: serviceHandle,
            origin: origin,
            params: params,
            local: local
        };
    };
    Core.prototype.registerNode = function () {
        //
    };
    ////////////////////////// IPC Event Handler Methods //////////////////////////
    /**
     * Takes in all the IPC messages, and delegates based on the event type
     * @param msg this is the message taken in, as a string (to be JSON-ified)
     */
    Core.prototype.handleIPCMessage = function (msg) {
        var message = JSON.parse(msg);
        if (!message['type']) {
            return this.handleUnknownMessage(message['data']);
        }
        else {
            switch (message['type']) {
                case 'register': {
                    return this.handleRegisterNode(message['data']);
                }
                case 'publish': {
                    return this.handlePublishMessage(message['data']);
                }
                case 'subscription': {
                    return this.handleSubscriptionMessage(message['data']);
                }
                case 'request': {
                    return this.handleRequest(message['data']);
                }
                case 'register-service': {
                    return this.handleRegisterService(message['data']);
                }
                default: {
                    return this.handleUnknownMessage(message['data']);
                }
            }
        }
    };
    /**
     *
     * @param msg
     */
    Core.prototype.handleRegisterNode = function (msg) {
        var _this = this;
        // note that the msg is ONLY the data portion of the message, so it must contain needed meta data
        var nodeHandle = msg['nodeHandle'];
        this.registerNode();
        if (this.logging) {
            console.log('[Core] recieved a register message from ' + nodeHandle);
        }
        this.publishEvent('system', 'new node registered: ' + nodeHandle);
        // save any services returned 
        var availableServices = [];
        if (msg['availableServices']) {
            availableServices = msg['availableServices'];
            console.log('available services: ' + availableServices);
            availableServices.forEach(function (service) {
                _this.registerService(service['nodeHandle'], service['origin'], service['params'], true);
            });
        }
        var subscriptions = [];
        if (msg['subscriptions']) {
            subscriptions = msg['subscriptions'];
            if (subscriptions.length > 0) {
                subscriptions.forEach(function (eventHandle) {
                    if (!_this.subscriptions[eventHandle]) {
                        _this.newEventBus(eventHandle);
                    }
                });
            }
        }
        if (this.localNodes[nodeHandle]) {
            if (msg['available_services']) {
                this.localNodes[nodeHandle]['available_services'] = availableServices;
            }
            if (msg['subscriptions']) {
                this.localNodes[nodeHandle]['subscriptions'] = subscriptions;
            }
        }
        else {
            this.localNodes[nodeHandle] = {
                available_services: availableServices,
                subscriptions: subscriptions
            };
        }
        return ((new cortex.AckMessageBuilder('core'))
            .returnMessageString());
    };
    /**
     *
     * @param msg
     */
    Core.prototype.handlePublishMessage = function (msg) {
        var eventHandle = msg['event'];
        var messageContent = msg['message'];
        if (this.subscriptions[eventHandle]) {
            // this event is already something we have a socket for, yay!
        }
        else {
            this.newEventBus(eventHandle);
        }
        this.publishEvent(eventHandle, messageContent);
        return (new cortex.AckMessageBuilder('core')).returnMessageString();
    };
    /**
     *
     * @param msg
     */
    Core.prototype.handleSubscriptionMessage = function (msg) {
        //handle the information
        if (this.logging) {
            console.log('[Core] recieved a new subscription message: ' + JSON.stringify(msg));
        }
        this.newEventBus(msg['eventHandle']);
        return (new cortex.AckMessageBuilder(this.CORE_ID)).returnMessageString();
    };
    /**
     *
     * @param msg
     */
    Core.prototype.handleRequest = function (msg) {
        //todo handle this
        // if the request type is 'get local nodes' => return list of nodes
        // if the request type is 'get local services' => return list of services
        // if the request type is 'get subscribe to this: X' => sub + ack (this should be limited to once)
        //      There should be a level of consistency across nodes and their messages, so things are sent once?
        // if the request type is 'new publish event' => publish to the right event on IPC
        //
        return this.handleUnknownMessage(msg);
    };
    /**
     *
     * @param msg
     */
    Core.prototype.handleRegisterService = function (msg) {
        //
        if (this.logging) {
            this.registerService(msg['serviceHandle'], msg['origin'], msg['params'], true);
        }
        return ((new cortex.AckMessageBuilder('core'))
            .returnMessageString());
    };
    /**
     *
     * @param message
     */
    Core.prototype.handleUnknownMessage = function (message) {
        if (this.logging) {
            console.log('[core] unknown message: ' + JSON.stringify(message));
        }
        return ((new cortex.WarnMessageBuilder('core'))
            .warning('unknown')
            .returnMessageString());
    };
    ////////////////////////// PRIVATE METHODS //////////////////////////
    // INIT METHODS
    /**
     * This establishes the parent process, and adds callbacks for process events, such as SIGINT
     */
    Core.prototype.establishProcess = function () {
        var _this = this;
        // Set up the safe ending when the process is killed
        process.on('SIGINT', function () {
            console.log('\n ending \n');
            _this.end();
        });
        process.on('SIGTERM', function () {
            console.log('\n ending \n');
            _this.end();
        });
    };
    /**
     * This handles configuration, and sets object fields based on the config object
     * @param config
     */
    Core.prototype.configure = function (config) {
        /////// MANIPULATE THE CONFIG OBJECT AS NEEDED 
        //todo accept a version number?
        this.config = config;
        /////// SET UP THE CONFIGURATION FOR JONES WORK
        // Check for CORE ID
        if (this.config['jones']['core_id']) {
            console.log('JONES INITIALIZATION - Found Core ID');
            this.CORE_ID = this.config['jones']['core_id'];
            console.log('[Core] setting core id: ', this.CORE_ID);
        }
        else {
            //this is what its set to on init
            console.log('JONES INITIALIZATION - NO Found Core ID');
            if (this.CORE_ID == '') {
                this.CORE_ID = 'default_core';
                console.log('[Core] setting core id: ', this.CORE_ID);
            }
            else {
                //do nothing, it has something other than init
                console.log('[Core] preserving old core id: ', this.CORE_ID);
            }
        }
        // Check for PATH
        if (this.config['jones']['path']) {
            console.log('JONES INITIALIZATION - Found path');
            this.path = this.config['jones']['path'];
            console.log('[JONES CORE] setting path: ', this.path);
        }
        else {
            console.log('JONES INITIALIZATION - NO Found path');
            if (this.path == '') {
                this.path = '/User/Michael/.jones/';
                console.log('[JONES CORE] setting path: ', this.path);
            }
            else {
                //do nothing, it has something other than init
                console.log('[JONES CORE] preserving old path: ', this.path);
            }
        }
        if (this.config['discovery']['mdns'] == true) {
            this.usemDNS = true;
        }
        else {
            this.usemDNS = false;
        }
        /////// SET UP THE CONFIGURATION FOR IPC
        if (this.config['ipc']['address']) {
            console.log('JONES INITIALIZATION - Found IPC address');
            this.ipcAddress = this.config['ipc']['address'];
            console.log('[JONES CORE] setting IPC address: ', this.ipcAddress);
        }
        else {
            console.log('JONES INITIALIZATION - NO Found IPC Address');
            if (this.ipcAddress == '') {
                this.ipcAddress = '/tmp/jones_core.ipc';
                console.log('[Core] setting IPC address: ', this.ipcAddress);
            }
            else {
                console.log('[Core] preserving old IPC address: ', this.ipcAddress);
            }
        }
        /////// SET UP THE Environment
        if (this.config['debug']['log']) {
            if (this.config['debug']['log'] == true) {
                this.logging = true;
            }
        }
        else {
            this.logging = false;
        }
    };
    return Core;
}(abstract_core_1.AbstractCore));
exports.Core = Core;
//# sourceMappingURL=core.js.map