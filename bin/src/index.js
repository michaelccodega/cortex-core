"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("./core");
var fs = require("fs");
//pass in the config; // one day this should be loaded from file
var configurationFile = fs.readFileSync('~/.jones/config.json').toString();
var core = new core_1.Core({
    path: '/Users/Michael/Workspaces/GitHubRepos/CortexCore/',
    //ipc_addr:'tcp://127.0.0.1:3000',
    ipc_addr: 'ipc:///tmp/jones_core.ipc',
    localTransport: 'ipc'
});
core.run();
setTimeout(function () {
    core.end();
}, 250000);
// When this just becomes a runnable, it should simply start, and go
// it should do things like load the config from file 
//# sourceMappingURL=index.js.map