"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//Imports
var nano = require("nanomsg");
var fs = require("fs");
//Main Core
var Core = /** @class */ (function () {
    //Constructor 
    function Core(config) {
        var _this = this;
        //Fields for local nodes
        /*
            Local Nodes include:
            name: string
            availableServices: Array<string>
            subscriptions: Array<string>
        */
        this.localNodes = {};
        this.localServices = [];
        //Fields for Events
        /*
            Subscriptions includes:
            eventHandle
            socketAddress
            socket
        */
        this.subscriptions = {};
        //Fields for Core to Core communication
        this.cores = [];
        //basic stuff:
        this.establishProcess();
        //handle the configs
        this.configure(config);
        //Create listener for NanoMsg - Local
        this.freeIPC(this.ipcAddress);
        this.ipcSocket = nano.socket('rep');
        this.ipcSocket.bind(this.ipcAddress);
        //this.ipcSocket.bind('tcp://127.0.0.1:3000');
        this.ipcSocket.on('data', function (data) {
            //console.log('[JONES CORE] incoming message: ' + data.toString() + '\n');
            _this.ipcSocket.send(_this.handleIPCMessage(data.toString()));
        });
    }
    ///////////// PUBLIC METHODS 
    Core.prototype.run = function () {
        //what does this do?
        console.log("Starting the CortexCore... \n\n");
    };
    Core.prototype.end = function () {
        //
        this.closeIPCListener();
        //end proc.
        process.exit(0);
    };
    ///////////// PRIVATE METHODS
    // INIT METHODS
    Core.prototype.establishProcess = function () {
        var _this = this;
        // Set up the safe ending when the process is killed
        process.on('SIGINT', function () {
            console.log("\n ending \n");
            _this.end();
        });
    };
    Core.prototype.configure = function (config) {
        /////// MANIPULATE THE CONFIG OBJECT AS NEEDED 
        //todo accept a version number?
        this.config = config;
        /////// SET UP THE CONFIGURATION OBJECT WITH DEFAULTS AS NEEDED
        // IPC SETTINGS
        if (this.config['ipc_addr']) {
            this.ipcAddress = this.config["ipc_addr"];
        }
        else {
            if (this.config['localTransport']) {
                if (this.config['localTransport'] == 'tcp') {
                    this.ipcAddress = 'tcp://127.0.0.1:3000';
                }
                if (this.config['localTransport'] == 'ipc') {
                    this.ipcAddress = 'ipc:///tmp/jones_core.ipc';
                }
            }
        }
    };
    Core.prototype.closeIPCListener = function () {
        this.freeIPC(this.ipcAddress);
        this.ipcSocket.shutdown(this.ipcAddress);
        this.ipcSocket.close();
    };
    // ACTION METHODS
    Core.prototype.handleIPCMessage = function (msg) {
        //this should somehow get used to set up future messaging over the CortexCore
        var message = JSON.parse(msg);
        if (!message['type']) {
            console.log('[JONES CORE] recieved message of unknown type');
            return JSON.stringify({
                type: 'warn',
                warn: 'unknown message type'
            });
        }
        else {
            switch (message['type']) {
                case 'register_msg': {
                    return this.registerNode(message['data']);
                }
                case 'subscription': {
                    return this.subscribeMessage(message['data']);
                }
                case 'foward': {
                    //todo //the actual message to be sent should be in: 
                    //           data:{ ..., fowarded_msg: {_this object_}, ... }
                    return JSON.stringify({
                        warn: 'not yet handled'
                    });
                }
                default: {
                    console.log('[JONES CORE] recieved message of unknown type');
                    console.log('[JONES CORE] returninig a warn ');
                    console.log('[JONES CORE] here is the offending message: ' + JSON.stringify(message));
                    return JSON.stringify({
                        type: 'warn',
                        warn: 'unknown'
                    });
                }
            }
        }
    };
    // MESSAGE EVENTS
    Core.prototype.registerNode = function (msg) {
        var _this = this;
        // note that the msg is ONLY the data portion of the message, so it must contain needed meta data
        var nodeName = msg['name'];
        console.log('[JONES CORE] recieved a register message from ' + nodeName);
        var availableServices = [];
        if (msg['available_services']) {
            availableServices = msg['available_services'];
        }
        var subscriptions = [];
        if (msg['subscriptions']) {
            subscriptions = msg['subscriptions'];
            subscriptions.forEach(function (eventHandle) {
                if (!_this.subscriptions[eventHandle]) {
                    _this.newEventBus(eventHandle);
                }
            });
        }
        // it can be used for updating stuff, so it will only update stuff it contains
        // todo: this can be simplified with the npm package 'xtend' (i think...)
        if (this.localNodes[nodeName]) {
            if (msg['available_services']) {
                this.localNodes[nodeName]['available_services'] = availableServices;
            }
            if (msg['subscriptions']) {
                this.localNodes[nodeName]['subscriptions'] = subscriptions;
            }
        }
        else {
            this.localNodes[nodeName] = {
                available_services: availableServices,
                subscriptions: subscriptions
            };
        }
        // as new things become available, they will get added to this
        return JSON.stringify({
            type: 'ack_msg',
            origin: 'core',
            msg_id: '0',
            event: 'private',
            recipient: nodeName,
            data: 'ack register'
        });
    };
    Core.prototype.subscribeMessage = function (msg) {
        console.log(msg);
        //handle the information
        this.newEventBus(msg['subscription']);
        return JSON.stringify({
            "type": "ack_msg",
            "origin": "core",
            "msg_id": "0",
            "data": {}
        });
    };
    Core.prototype.handleEvent = function (eventHandle, msg) {
        var message = msg.toString();
        console.log('[JONES CORE] ' + eventHandle + ': ' + message);
        // filter this message if needed
        // pass this event on to other cores
    };
    // HELPER METHODS
    Core.prototype.freeIPC = function (ipc) {
        try {
            //ensure that the IPC is there, then remove it
            if (fs.existsSync(this.config["path"] + ipc)) {
                fs.unlinkSync(this.config["path"] + ipc);
            }
            else {
                if (fs.existsSync('/tmp/jones_core.ipc')) {
                    fs.unlinkSync('/tmp/jones_core.ipc');
                }
                return;
            }
        }
        catch (e) {
            // it isnt there
            console.log(e);
            return;
        }
        return;
    };
    Core.prototype.newEventBus = function (eventHandle) {
        var _this = this;
        if (this.subscriptions[eventHandle]) {
            return;
        }
        console.log('[JONES CORE] Registered Subscription: ' + eventHandle);
        var socket = nano.socket('bus');
        var address = 'ipc:///tmp/jones_core_event_' + eventHandle + '_.ipc';
        socket.connect(address);
        socket.send('listening');
        socket.on('data', function (message) {
            console.log('[JONES CORE] handling a socket message');
            _this.handleEvent(eventHandle, message);
        });
        this.subscriptions[eventHandle] = {
            eventHandle: eventHandle,
            address: address,
            socket: socket
        };
    };
    return Core;
}());
exports.Core = Core;
//# sourceMappingURL=core.js.map