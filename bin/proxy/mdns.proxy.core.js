"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
//Imports
var abstract_proxy_core_1 = require("./abstract.proxy.core");
var bonjour = require("bonjour");
/*

This should find any other TCP Cores on the network, and expose them as TCP Proxy's

Then, it publishes itself.  It creates a TCP listner in the mDNS proxy, and that handles incoming TCP Messages

*/
var mDnsProxy = /** @class */ (function (_super) {
    __extends(mDnsProxy, _super);
    // Constructor
    function mDnsProxy(config, core) {
        var _this = _super.call(this, core) || this;
        _this.debugLog = false;
        _this.config = config;
        _this.configure();
        _this.id = 'cortex-core-' + coreHandle;
        _this.bonjour = bonjour();
        _this.bonjour.publish({
            name: _this.id,
            type: 'cortex-core',
            port: 9876,
            txt: {}
        });
        return _this;
    }
    mDnsProxy.prototype.spin = function () {
        var _this = this;
        /*
            This repeats once a second, looking for new cores
        */
        setInterval(function () {
            _this.bonjour.find({ type: 'cortex-core' }, function (service) {
                if (service['name'] == _this.id) {
                    //
                }
                else {
                    if (_this.debugLog) {
                        console.log('\n[proxy-core] new Bonjour service discovered: ' + JSON.stringify(service));
                    }
                }
            });
        }, 1000);
    };
    mDnsProxy.prototype.configure = function () {
        if (this.config['debug']['log'] == true) {
            this.debugLog = true;
        }
        else {
            this.debugLog = false;
        }
    };
    return mDnsProxy;
}(abstract_proxy_core_1.AbstractProxy));
exports.mDnsProxy = mDnsProxy;
//# sourceMappingURL=mdns.proxy.core.js.map