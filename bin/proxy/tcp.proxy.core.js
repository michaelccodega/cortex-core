"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
//
var abstract_proxy_core_1 = require("./abstract.proxy.core");
var nano = require("nanomsg");
var TcpProxy = /** @class */ (function (_super) {
    __extends(TcpProxy, _super);
    //Constructor
    function TcpProxy(config, core, tcpAddress) {
        var _this = _super.call(this, core) || this;
        _this.configure(config);
        _this.address = tcpAddress;
        _this.socket = nano.socket('req');
        _this.socket.connect('tcp://' + _this.address);
        _this.socket.on('data', function (data) {
            _this.handleMessage(data.toString());
        });
        _this.requestInfo();
        return _this;
    }
    TcpProxy.prototype.requestInfo = function () {
        //
    };
    TcpProxy.prototype.sendMessage = function (message) {
        this.socket.send(message.toString());
    };
    TcpProxy.prototype.handleMessage = function (data) {
        //
        //add a switch statement here
    };
    ////////////////////////////////////////////////////////////////
    /**
     *
     * @param config
     */
    TcpProxy.prototype.configure = function (config) {
        //
    };
    return TcpProxy;
}(abstract_proxy_core_1.AbstractProxy));
exports.TcpProxy = TcpProxy;
//# sourceMappingURL=tcp.proxy.core.js.map