"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var abstract_core_1 = require("../abstract.core");
//
var AbstractProxy = /** @class */ (function (_super) {
    __extends(AbstractProxy, _super);
    // Fields
    // Constructor
    function AbstractProxy(core) {
        return _super.call(this) || this;
    }
    return AbstractProxy;
}(abstract_core_1.AbstractCore));
exports.AbstractProxy = AbstractProxy;
//# sourceMappingURL=abstract.proxy.core.js.map