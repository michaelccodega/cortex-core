"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
//
var node_1 = require("../core/node");
var service_1 = require("../core/service");
//
var nano = require("nanomsg");
var cortex = require("lib-cortex");
var TCPManager = /** @class */ (function () {
    //constructor
    function TCPManager(config, core) {
        var _this = this;
        // Public Fields
        this.CORE_ID = '';
        this.path = '';
        //Fields for Core
        this.id = 'tcp';
        this.config = {};
        this.logging = false;
        //Fields for NanoMsg IPC Socket
        this.tcpAddress = '';
        this.core = core;
        this.configure(config);
        // Make a socket
        this.tcpAddress = 'tcp://127.0.0.1:5678';
        this.tcpSocket = nano.socket('rep');
        this.tcpSocket.bind(this.tcpAddress);
        this.tcpSocket.on('data', function (data) {
            _this.tcpSocket.send(_this.handleMessage(data.toString()));
        });
        this.core.on('event', function (eventHandle, message, sender) {
            //sender? - IPC is special?
            if (sender == _this.id) {
                //
            }
            else {
                _this.publishEvent(eventHandle, message);
            }
        });
    }
    TCPManager.prototype.end = function () {
        //this should free all the other addresses associated with subscriptions and services
    };
    //////////////////////////  Methods //////////////////////////
    /**
     *
     * @param msg
     */
    TCPManager.prototype.handleMessage = function (msg) {
        if (this.logging) {
            console.log('\nCore recieved a new message: ');
            console.log('   ' + msg);
            console.log('');
        }
        var message;
        try {
            message = JSON.parse(msg);
        }
        catch (error) {
            return this.handleUnknownMessage({});
        }
        if (!message['type']) {
            return this.handleUnknownMessage(message['data']);
        }
        else {
            switch (message['type']) {
                case 'register': {
                    return this.handleRegisterNode(message['data']);
                }
                case 'publish': {
                    return this.handlePublishMessage(message['data']);
                }
                case 'subscription': {
                    return this.handleSubscriptionMessage(message['data']);
                }
                case 'request': {
                    return this.handleRequest(message['data']);
                }
                case 'register-service': {
                    return this.handleRegisterService(message['data']);
                }
                default: {
                    return this.handleUnknownMessage(message['data']);
                }
            }
        }
    };
    //
    /**
     *
     * @param config
     */
    TCPManager.prototype.configure = function (config) {
        this.config = config;
        //
    };
    /**
     *
     * @param eventHandle
     * @param message
     */
    TCPManager.prototype.publishEvent = function (eventHandle, message) {
        //send to the cores that it is responsible for
    };
    ////////////////////////// IPC Event Handler Methods //////////////////////////
    /**
     *
     * @param msgData
     */
    TCPManager.prototype.handleRegisterNode = function (msgData) {
        if (this.logging) {
            console.log('[Core] recieved a register message from ' + msgData['nodeHandle']);
        }
        var node = new TCPNode(msgData['nodeHandle'], msgData['uuid'], msgData['parentCoreId']);
        this.core.registerNode(node);
        return ((new cortex.AckMessageBuilder(this.core.CORE_ID))
            .returnMessageString());
    };
    /**
     *
     * @param msg
     */
    TCPManager.prototype.handlePublishMessage = function (msg) {
        var eventHandle = msg['event'];
        var messageContent = msg['message'];
        this.core.publishLocalEvent(eventHandle, messageContent, this.id, true);
        return (new cortex.AckMessageBuilder('core')).returnMessageString();
    };
    /**
     *
     * @param msg
     */
    TCPManager.prototype.handleSubscriptionMessage = function (msg) {
        //handle the information
        return (new cortex.AckMessageBuilder(this.CORE_ID)).returnMessageString();
    };
    /**
     *
     * @param msg
     */
    TCPManager.prototype.handleRequest = function (msg) {
        //TODO handle this
        // if the request type is 'get local nodes' => return list of nodes
        // if the request type is 'get local services' => return list of services
        // if the request type is 'get subscribe to this: X' => sub + ack (this should be limited to once)
        //      There should be a level of consistency across nodes and their messages, so things are sent once?
        // if the request type is 'new publish event' => publish to the right event on IPC
        //
        if (msg['type'] == 'request') {
            this.core.messageService(msg['destination'], cortex.Message.Parse(msg.toString()));
        }
        return this.handleUnknownMessage(msg);
    };
    /**
     *
     * @param msg
     */
    TCPManager.prototype.handleRegisterService = function (msg) {
        //
        if (this.logging) {
            //
        }
        var service = new TCPService(msg['serviceHandle'], msg['params'], msg['parentNode']);
        this.core.registerService(service);
        return ((new cortex.AckMessageBuilder('core'))
            .returnMessageString());
    };
    /**
     *
     * @param msg
     */
    TCPManager.prototype.handleUnknownMessage = function (message) {
        if (this.logging) {
            console.log('[core] unknown message: ' + JSON.stringify(message));
        }
        return ((new cortex.WarnMessageBuilder('core'))
            .warning('unknown')
            .returnMessageString());
    };
    return TCPManager;
}());
exports.TCPManager = TCPManager;
var TCPNode = /** @class */ (function (_super) {
    __extends(TCPNode, _super);
    function TCPNode(nodeHandle, uuid, coreId) {
        var _this = 
        //TODO: change this to not be for IPC
        _super.call(this, nodeHandle, uuid, [], coreId) || this;
        _this.address = cortex.IPCUtils.encodeAddressNode(_this.handle);
        _this.socket = cortex.SocketUtils.createSocketSlave('req', _this.address);
        return _this;
    }
    TCPNode.prototype.sendMessage = function (message) {
        this.socket.send(message.toString());
    };
    return TCPNode;
}(node_1.Node));
exports.TCPNode = TCPNode;
var TCPService = /** @class */ (function (_super) {
    __extends(TCPService, _super);
    function TCPService(serviceHandle, params, parentNode) {
        var _this = 
        //TODO: change this to not be for IPC
        _super.call(this, serviceHandle, params, parentNode) || this;
        _this.address = cortex.IPCUtils.encodeAddressService(_this.serviceHandle);
        _this.socket = cortex.SocketUtils.createSocketSlave('req', _this.address);
        return _this;
    }
    TCPService.prototype.sendMessage = function (message) {
        //this.socket.send(message.toString());
        var msg = new cortex.ForwardMessageBuilder('tcp')
            .setMessage(message)
            .returnMessage();
    };
    return TCPService;
}(service_1.Service));
exports.TCPService = TCPService;
//# sourceMappingURL=tcp.manager.js.map