"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
//
var node_1 = require("../core/node");
var service_1 = require("../core/service");
var cortex = require("lib-cortex");
var IPCManager = /** @class */ (function () {
    // Constructor
    function IPCManager(config, core) {
        var _this = this;
        // Public Fields
        this.CORE_ID = '';
        this.path = '';
        //Fields for Core
        this.id = 'ipc';
        this.config = {};
        this.logging = false;
        //Fields for NanoMsg IPC Socket
        this.ipcAddress = '';
        this.eventBuses = {};
        this.core = core;
        this.configure(config);
        this.path = this.core.path;
        this.eventBuses = {};
        // Establish IPC
        cortex.SocketUtils.freeIPCfile(this.ipcAddress, this.core.path);
        this.ipcSocket = cortex.SocketUtils.createSocketMaster('rep', this.ipcAddress);
        this.ipcSocket.on('data', function (data) {
            //console.log('\'' + data.toString() + '\'');
            _this.ipcSocket.send(_this.handleMessage(data.toString()));
        });
        this.core.on('event', function (eventHandle, message, sender) {
            //sender? - IPC is special?
            if (sender == 'ipc') {
                //
            }
            else {
                //
            }
            _this.handleLocalPublishEvent(eventHandle, message);
        });
        this.core.on('event-local', function (eventHandle, message) {
            //sender :/
            _this.handleLocalPublishEvent(eventHandle, message);
        });
    }
    IPCManager.prototype.end = function () {
        this.ipcSocket.shutdown(cortex.SocketUtils.pathToSocketAddress(this.ipcAddress));
        this.ipcSocket.close();
        cortex.SocketUtils.freeIPCfile(this.ipcAddress, this.path);
        //this should free all the other addresses associated with subscriptions and services
    };
    //////////////////////////  Methods //////////////////////////
    /**
     * Takes in all the IPC messages, and delegates based on the event type
     * @param msg this is the message taken in, as a string (to be JSON-ified)
     */
    IPCManager.prototype.handleMessage = function (msg) {
        if (this.logging) {
            console.log('\nCore recieved a new message: ');
            console.log('   ' + msg);
            console.log('');
        }
        var message;
        try {
            message = JSON.parse(msg);
        }
        catch (error) {
            return this.handleUnknownMessage({});
        }
        if (!message['type']) {
            return this.handleUnknownMessage(message['data']);
        }
        else {
            switch (message['type']) {
                case 'register': {
                    return this.handleRegisterNode(message['data']);
                }
                case 'publish': {
                    return this.handlePublishMessage(message['data']);
                }
                case 'subscription': {
                    return this.handleSubscriptionMessage(message['data']);
                }
                case 'request': {
                    return this.handleRequest(message['data']);
                }
                case 'register-service': {
                    return this.handleRegisterService(message['data']);
                }
                default: {
                    return this.handleUnknownMessage(message['data']);
                }
            }
        }
    };
    IPCManager.prototype.configure = function (config) {
        /////// MANIPULATE THE CONFIG OBJECT AS NEEDED 
        //TODO accept a version number?
        this.config = config;
        /////// SET UP THE CONFIGURATION FOR IPC
        if (this.config['ipc']['address']) {
            console.log('JONES INITIALIZATION - Found IPC address');
            this.ipcAddress = this.config['ipc']['address'];
            console.log('[JONES CORE] setting IPC address: ', this.ipcAddress);
        }
        else {
            console.log('JONES INITIALIZATION - NO Found IPC Address');
            if (this.ipcAddress == '') {
                this.ipcAddress = '/tmp/jones_core.ipc';
                console.log('[Core] setting IPC address: ', this.ipcAddress);
            }
            else {
                console.log('[Core] preserving old IPC address: ', this.ipcAddress);
            }
        }
        /////// Set up the Environment
        if (this.config['debug']['log']) {
            if (this.config['debug']['log'] == true) {
                this.logging = true;
                console.log('logging is enabled');
            }
        }
        else {
            this.logging = false;
        }
    };
    IPCManager.prototype.newEventBus = function (eventHandle) {
        var _this = this;
        if (this.logging) {
            console.log('[Core] creating a new EventBus for: ' + eventHandle);
        }
        var address = cortex.IPCUtils.encodeAddressEvent(eventHandle);
        var socket = cortex.SocketUtils.createSocketMaster('bus', address);
        socket.on('data', function (message) {
            //this should never trigger? // Since no where to SLAVE sockets send messages
            if (_this.logging) {
                console.log(message);
            }
        });
        this.eventBuses[eventHandle] = {
            eventHandle: eventHandle,
            address: address,
            socket: socket
        };
    };
    /**
     * This sends the published message to the eventBus's. It is the handler that is passed into the P.Core
     * @param eventHandle
     * @param message
     */
    IPCManager.prototype.handleLocalPublishEvent = function (eventHandle, message) {
        this.eventBuses[eventHandle].socket.send(message.toString());
        console.log('eventBus Address: ' + this.eventBuses[eventHandle]['address']);
        if (this.logging) {
            console.log('Core - Publishing Event ' + eventHandle + ' @= ' + message.toString());
            console.log('event bus is: ' + this.eventBuses[eventHandle].address);
        }
    };
    ////////////////////////// IPC Event Handler Methods //////////////////////////
    /**
     *
     * @param msgData
     */
    IPCManager.prototype.handleRegisterNode = function (msgData) {
        if (this.logging) {
            console.log('[Core] recieved a register message from ' + msgData['nodeHandle']);
        }
        var node = new IPCNode(msgData['nodeHandle'], msgData['uuid'], msgData['parentCoreId']);
        this.core.registerNode(node);
        return ((new cortex.AckMessageBuilder(this.core.CORE_ID))
            .returnMessageString());
    };
    /**
     * This takes in a publish message from the central IPC channel, and pushes it as a new event to the P.Core
     * @param msg the publish message
     */
    IPCManager.prototype.handlePublishMessage = function (msg) {
        /*

        This should tell the core about the message, though "Publish Message"
        It should be use the "local" to know when to publish locally

        */
        var eventHandle = msg['event'];
        var messageContent = msg['message'];
        //Generating an event bus for this message if it does not exist 
        // should this be done here, or in a handler
        if (this.eventBuses[eventHandle]) {
            // this event is already something we have a socket for, yay!
        }
        else {
            // Somewhere they should encode the event handle, but it should be as close to the socket address thing as possible
            this.newEventBus(eventHandle);
        }
        this.core.publishEvent(eventHandle, messageContent, this.id, true);
        return (new cortex.AckMessageBuilder('core')).returnMessageString();
    };
    /**
     *
     * @param msg
     */
    IPCManager.prototype.handleSubscriptionMessage = function (msg) {
        //handle the information
        if (this.logging) {
            console.log('[Core] recieved a new subscription message: ' + JSON.stringify(msg));
        }
        var eventHandle = msg['eventHandle'];
        if (this.eventBuses[eventHandle]) {
            // this event is already something we have a socket for, yay!
        }
        else {
            this.newEventBus(eventHandle);
        }
        return (new cortex.AckMessageBuilder(this.CORE_ID)).returnMessageString();
    };
    /**
     *
     * @param msg
     */
    IPCManager.prototype.handleRequest = function (msg) {
        //TODO handle this
        // if the request type is 'get local nodes' => return list of nodes
        // if the request type is 'get local services' => return list of services
        // if the request type is 'get subscribe to this: X' => sub + ack (this should be limited to once)
        //      There should be a level of consistency across nodes and their messages, so things are sent once?
        // if the request type is 'new publish event' => publish to the right event on IPC
        //
        if (msg['type'] == 'request') {
            this.core.messageService(msg['destination'], cortex.Message.Parse(msg.toString()));
        }
        return this.handleUnknownMessage(msg);
    };
    /**
     *
     * @param msg
     */
    IPCManager.prototype.handleRegisterService = function (msg) {
        //
        if (this.logging) {
            //
        }
        var service = new IPCService(msg['serviceHandle'], msg['params'], msg['parentNode']);
        this.core.registerService(service);
        return ((new cortex.AckMessageBuilder('core'))
            .returnMessageString());
    };
    /**
     *
     * @param message
     */
    IPCManager.prototype.handleUnknownMessage = function (message) {
        if (this.logging) {
            console.log('[core] unknown message: ' + JSON.stringify(message));
        }
        return ((new cortex.WarnMessageBuilder('core'))
            .warning('unknown')
            .returnMessageString());
    };
    return IPCManager;
}());
exports.IPCManager = IPCManager;
var IPCNode = /** @class */ (function (_super) {
    __extends(IPCNode, _super);
    function IPCNode(nodeHandle, uuid, coreId) {
        var _this = _super.call(this, nodeHandle, uuid, [], coreId) || this;
        _this.address = cortex.IPCUtils.encodeAddressNode(_this.handle);
        _this.socket = cortex.SocketUtils.createSocketSlave('req', _this.address);
        return _this;
    }
    IPCNode.prototype.sendMessage = function (message) {
        this.socket.send(message.toString());
    };
    return IPCNode;
}(node_1.Node));
exports.IPCNode = IPCNode;
var IPCService = /** @class */ (function (_super) {
    __extends(IPCService, _super);
    function IPCService(serviceHandle, params, parentNode) {
        var _this = _super.call(this, serviceHandle, params, parentNode) || this;
        _this.address = cortex.IPCUtils.encodeAddressService(_this.serviceHandle);
        _this.socket = cortex.SocketUtils.createSocketSlave('req', _this.address);
        return _this;
    }
    IPCService.prototype.sendMessage = function (message) {
        this.socket.send(message.toString());
    };
    return IPCService;
}(service_1.Service));
exports.IPCService = IPCService;
//# sourceMappingURL=ipc.manager.js.map