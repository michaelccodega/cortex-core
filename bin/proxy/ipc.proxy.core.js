"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
//
var abstract_proxy_core_1 = require("./abstract.proxy.core");
var cortex = require("lib-cortex");
var IPCManager = /** @class */ (function (_super) {
    __extends(IPCManager, _super);
    // Constructor
    function IPCManager(config, core) {
        var _this = _super.call(this, core) || this;
        //Fields for Core
        _this.id = 'ipc';
        //Fields for NanoMsg IPC Socket
        _this.ipcAddress = '';
        _this.eventBuses = {};
        _this.configure(config);
        _this.path = _this.core.path;
        _this.eventBuses = {};
        // Establish IPC
        cortex.SocketUtils.freeIPCfile(_this.ipcAddress, _this.core.path);
        _this.ipcSocket = cortex.SocketUtils.createSocketMaster('rep', _this.ipcAddress);
        _this.ipcSocket.on('data', function (data) {
            //console.log('\'' + data.toString() + '\'');
            _this.ipcSocket.send(_this.handleIPCMessage(data.toString()));
        });
        _this.core.on('event', function (eventHandle, message) {
            //sender? - IPC is special?
            _this.publishEvent(eventHandle, message);
        });
        _this.core.on('event-local', function (eventHandle, message) {
            //sender :/
            _this.publishEvent(eventHandle, message);
        });
        return _this;
    }
    IPCManager.prototype.end = function () {
        this.ipcSocket.shutdown(cortex.SocketUtils.pathToSocketAddress(this.ipcAddress));
        this.ipcSocket.close();
        cortex.SocketUtils.freeIPCfile(this.ipcAddress, this.path);
        //this should free all the other addresses associated with subscriptions and services
    };
    //////////////////////////  Methods //////////////////////////
    IPCManager.prototype.configure = function (config) {
        /////// MANIPULATE THE CONFIG OBJECT AS NEEDED 
        //todo accept a version number?
        this.config = config;
        /////// SET UP THE CONFIGURATION FOR IPC
        if (this.config['ipc']['address']) {
            console.log('JONES INITIALIZATION - Found IPC address');
            this.ipcAddress = this.config['ipc']['address'];
            console.log('[JONES CORE] setting IPC address: ', this.ipcAddress);
        }
        else {
            console.log('JONES INITIALIZATION - NO Found IPC Address');
            if (this.ipcAddress == '') {
                this.ipcAddress = '/tmp/jones_core.ipc';
                console.log('[Core] setting IPC address: ', this.ipcAddress);
            }
            else {
                console.log('[Core] preserving old IPC address: ', this.ipcAddress);
            }
        }
    };
    IPCManager.prototype.newEventBus = function (eventHandle) {
        var _this = this;
        if (this.logging) {
            console.log('[Core] creating a new EventBus for: ' + eventHandle);
        }
        var address = cortex.IPCUtils.encodeAddressEvent(eventHandle);
        var socket = cortex.SocketUtils.createSocketMaster('bus', address);
        socket.on('data', function (message) {
            //this should never trigger? // Since no where to SLAVE sockets send messages
            if (_this.logging) {
                console.log(message);
            }
        });
        this.eventBuses[eventHandle] = {
            eventHandle: eventHandle,
            address: address,
            socket: socket
        };
    };
    IPCManager.prototype.publishEvent = function (eventHandle, message) {
        this.eventBuses[eventHandle].socket.send(message.toString());
    };
    ////////////////////////// IPC Event Handler Methods //////////////////////////
    /**
     * Takes in all the IPC messages, and delegates based on the event type
     * @param msg this is the message taken in, as a string (to be JSON-ified)
     */
    IPCManager.prototype.handleIPCMessage = function (msg) {
        var message = JSON.parse(msg);
        if (!message['type']) {
            return this.handleUnknownMessage(message['data']);
        }
        else {
            switch (message['type']) {
                case 'register': {
                    return this.handleRegisterNode(message['data']);
                }
                case 'publish': {
                    return this.handlePublishMessage(message['data']);
                }
                case 'subscription': {
                    return this.handleSubscriptionMessage(message['data']);
                }
                case 'request': {
                    return this.handleRequest(message['data']);
                }
                case 'register-service': {
                    return this.handleRegisterService(message['data']);
                }
                default: {
                    return this.handleUnknownMessage(message['data']);
                }
            }
        }
    };
    /**
     *
     * @param msg
     */
    IPCManager.prototype.handleRegisterNode = function (msg) {
        // note that the msg is ONLY the data portion of the message, so it must contain needed meta data
        var nodeHandle = msg['nodeHandle'];
        if (this.logging) {
            console.log('[Core] recieved a register message from ' + nodeHandle);
        }
        this.core.publishEvent('system', 'new node registered: ' + nodeHandle, this.id);
        this.core.registerLocalNode(nodeHandle);
        return ((new cortex.AckMessageBuilder('core'))
            .returnMessageString());
    };
    /**
     *
     * @param msg
     */
    IPCManager.prototype.handlePublishMessage = function (msg) {
        var eventHandle = msg['event'];
        var messageContent = msg['message'];
        if (this.eventBuses[eventHandle]) {
            // this event is already something we have a socket for, yay!
        }
        else {
            this.newEventBus(eventHandle);
        }
        this.core.publishEvent(eventHandle, messageContent, this.id);
        return (new cortex.AckMessageBuilder('core')).returnMessageString();
    };
    /**
     *
     * @param msg
     */
    IPCManager.prototype.handleSubscriptionMessage = function (msg) {
        //handle the information
        if (this.logging) {
            console.log('[Core] recieved a new subscription message: ' + JSON.stringify(msg));
        }
        this.newEventBus(msg['eventHandle']);
        return (new cortex.AckMessageBuilder(this.CORE_ID)).returnMessageString();
    };
    /**
     *
     * @param msg
     */
    IPCManager.prototype.handleRequest = function (msg) {
        //todo handle this
        // if the request type is 'get local nodes' => return list of nodes
        // if the request type is 'get local services' => return list of services
        // if the request type is 'get subscribe to this: X' => sub + ack (this should be limited to once)
        //      There should be a level of consistency across nodes and their messages, so things are sent once?
        // if the request type is 'new publish event' => publish to the right event on IPC
        //
        return this.handleUnknownMessage(msg);
    };
    /**
     *
     * @param msg
     */
    IPCManager.prototype.handleRegisterService = function (msg) {
        //
        if (this.logging) {
            //
        }
        this.core.registerLocalService(msg['serviceHandle'], msg['origin'], msg['params']);
        return ((new cortex.AckMessageBuilder('core'))
            .returnMessageString());
    };
    /**
     *
     * @param message
     */
    IPCManager.prototype.handleUnknownMessage = function (message) {
        if (this.logging) {
            console.log('[core] unknown message: ' + JSON.stringify(message));
        }
        return ((new cortex.WarnMessageBuilder('core'))
            .warning('unknown')
            .returnMessageString());
    };
    return IPCManager;
}(abstract_proxy_core_1.AbstractProxy));
exports.IPCManager = IPCManager;
//# sourceMappingURL=ipc.proxy.core.js.map